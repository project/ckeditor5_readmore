<?php

/**
 * @file
 * Contains ckeditor5_readmore.module.
 */

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function ckeditor5_readmore_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.ckeditor5_readmore':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Allows to insert readmore container in CKEditor 5.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_library_info_alter().
 */
function ckeditor5_readmore_library_info_alter(&$libraries, $extension) {
  $module = 'ckeditor5_readmore';
  if ($extension === 'ckeditor5') {
    // Add paths to stylesheets specified by a modules's ckeditor5-stylesheets
    // config property.
    $module_path = \Drupal::service('extension.list.module')->getPath($module);
    $info = \Drupal::service('extension.list.module')->getExtensionInfo($module);
    if (isset($info['ckeditor5-stylesheets']) && $info['ckeditor5-stylesheets'] !== FALSE) {
      $css = $info['ckeditor5-stylesheets'];
      foreach ($css as $key => $url) {
        // CSS URL is external or relative to Drupal root.
        if (UrlHelper::isExternal($url) || $url[0] === '/') {
          $css[$key] = $url;
        }
        // CSS URL is relative to theme.
        else {
          $css[$key] = '/' . $module_path . '/' . $url;
        }
      }
    }
    $libraries['internal.drupal.ckeditor5.stylesheets'] = [
      'css' => [
        'theme' => array_fill_keys(array_values($css), []),
      ],
    ];
  }
}

/**
 * Implements hook_preprocess_html().
 */
function ckeditor5_readmore_preprocess_html(&$variables) {
  $variables['#attached']['library'][] = 'ckeditor5_readmore/frontend';
}
