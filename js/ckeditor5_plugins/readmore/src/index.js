/**
 * @file
 * Defines the ReadMore plugin.
 */

/**
 * @module readmore/ReadMore
 */

import { Plugin } from 'ckeditor5/src/core';
import ReadMoreEditing from './editing';
import ReadMoreUI from './ui';

/**
 * The ReadMore plugin.
 *
 * <div class="ckeditor-readmore"
 *   data-readmore-less-text="Read less"
 *   data-readmore-more-text="Read more"
 *   data-readmore-type="text|button"></div>
 *
 * This is a "glue" plugin that loads
 * @extends module:core/plugin~Plugin
 */
class ReadMore extends Plugin {

  /**
   * @inheritdoc
   */
  static get requires() {
    return [ReadMoreEditing, ReadMoreUI];
  }

  /**
   * @inheritdoc
   */
  static get pluginName() {
    return 'readmore';
  }

}

export default { ReadMore };
