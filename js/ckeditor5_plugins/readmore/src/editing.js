/**
 * @file
 * Defines the ReadMoreEditing plugin.
 */

/**
 * @module readmore/ReadMoreEditing
 */

import {Plugin} from 'ckeditor5/src/core';
import ReadMoreCommand from "./command";
import ReadMoreRemoveCommand from "./commandRemove";

/**
 * The editing feature.
 *
 * It introduces the 'readmore' element in the model.
 *
 * @extends module:core/plugin~Plugin
 */
export default class ReadMoreEditing extends Plugin {

  /**
   * @inheritDoc
   */
  init() {
    this._defineSchema();
    this._defineConverters();

    const editor = this.editor;

    // Attaching the command to the editor.
    editor.commands.add(
      'readmore',
      new ReadMoreCommand(this.editor),
    );
    editor.commands.add(
      'readmoreremove',
      new ReadMoreRemoveCommand(this.editor),
    );
  }

  /**
   * Registers schema for bbutton and its child elements.
   *
   * @private
   */
  _defineSchema() {
    const schema = this.editor.model.schema;

    // parent element.
    schema.register('readmore', {
      inheritAllFrom: '$root',
      allowIn: '$root',
      allowAttributes: [ 'class', 'data-readmore-less-text', 'data-readmore-more-text', 'data-readmore-type' ],
      allowContentOf: '$root'
    });

  }

  /**
   * Defines converters.
   */
  _defineConverters() {
    const {conversion} = this.editor;

    // View -> Model.
    conversion.for('upcast').elementToElement({
      view: {
        name: 'div',
        classes: [ 'ckeditor-readmore' ]
      },
      converterPriority: 'high',
      model: (viewElement, conversionApi ) => {

        const attrs = {
          class: 'ckeditor-readmore',
          'data-readmore-less-text': viewElement.getAttribute('data-readmore-less-text'),
          'data-readmore-more-text': viewElement.getAttribute('data-readmore-more-text'),
          'data-readmore-type': viewElement.getAttribute('data-readmore-type'),
        };

        return conversionApi.writer.createElement( 'readmore', attrs );
      },
    });

    // Model -> View.
    conversion.for('downcast').elementToElement({
      model: 'readmore',
      view: (modelElement, { writer }) => {

        const htmlAttrs = {
          'class': 'ckeditor-readmore',
          'data-readmore-less-text': modelElement.getAttribute('data-readmore-less-text'),
          'data-readmore-more-text': modelElement.getAttribute('data-readmore-more-text'),
          'data-readmore-type': modelElement.getAttribute('data-readmore-type'),
        };

        return writer.createContainerElement('div', htmlAttrs);
      }

    });
  }
}
