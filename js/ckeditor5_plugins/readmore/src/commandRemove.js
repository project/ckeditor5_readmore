/**
 * @file
 * Defines the ReadMoreCommandRemove plugin.
 */

import { Command } from 'ckeditor5/src/core';
import {
  findElement,
} from "./utils";

/**
 * The ReadMore Remove command.
 *
 * @extends module:core/command~Command
 */
export default class ReadMoreRemoveCommand extends Command {

  /**
   * @inheritDoc
   */
  refresh() {
    // Init the empty command value.
    this.value = null;

    // Find the element in the selection.
    const { selection } = this.editor.model.document;
    const El = findElement(selection, 'readmore');
    if (!El) {
      this.isEnabled = false;
    } else {
      this.isEnabled = true;
    }
  }

  /**
   * @inheritDoc
   */
  execute(values) {
    const { model } = this.editor;
    const editor = this.editor;
    var El = findElement(model.document.selection, 'readmore');

    const docFrag = model.change((writer) => {
      // Find an existing readmore if it is being edited.
      const p = writer.createDocumentFragment();
      if (El) {
        for ( const element of El.getChildren() ) {
          writer.append(writer.cloneElement(element, true), p);
        }
      }
      writer.setSelection(El, 'on');
      return p;
    });

    editor.model.insertContent(docFrag);
  }
}
