/**
 * @file
 * Defines the ReadMoreUI plugin.
 */

/**
 * @module readmore/ReadMoreUI
 */

import { Plugin } from 'ckeditor5/src/core';
import {
  ButtonView,
  ContextualBalloon,
} from 'ckeditor5/src/ui';
import Icon from '../../../icons/rm.svg';
import IconRemove from '../../../icons/rm-remove.svg';

/**
 * The UI plugin. It introduces the `'readmore'` buttons.
 *
 * It uses the
 * {@link module:ui/panel/balloon/contextualballoon~ContextualBalloon contextual balloon plugin}.
 *
 * @extends module:core/plugin~Plugin
 */
export default class ReadMoreUI extends Plugin {

  /**
   * @inheritDoc
   */
  static get requires() {
    return [ ContextualBalloon ];
  }

  /**
   * @inheritDoc
   */
  init() {
    // Create the buttons.
    this._addToolbarButtons();
  }

  /**
   * Adds the toolbar buttons.
   *
   * @private
   */
  _addToolbarButtons() {
    const editor = this.editor;

    editor.ui.componentFactory.add('ReadMore', (locale) => {
      const buttonView = new ButtonView(locale);

      // Create the toolbar button.
      buttonView.set({
        label: editor.t('Read More'),
        icon: Icon,
        tooltip: true
      });

      // Bind button to the command.
      // The state on the button depends on the command values.
      const command = editor.commands.get('readmore');
      buttonView.bind( 'isEnabled' ).to( command, 'isEnabled' );
      buttonView.bind( 'isOn' ).to( command, 'value', value => !!value );

      // Execute the command when the button is clicked.
      this.listenTo(buttonView, 'execute', () =>
        editor.execute('readmore', {})
      );
      return buttonView;
    });

    editor.ui.componentFactory.add('ReadMoreRemove', (locale) => {
      const buttonView = new ButtonView(locale);

      // Create the toolbar button.
      buttonView.set({
        label: editor.t('Read More Remove'),
        icon: IconRemove,
        tooltip: true
      });

      // Bind button to the command.
      // The state on the button depends on the command values.
      const command = editor.commands.get('readmoreremove');
      buttonView.bind( 'isEnabled' ).to( command, 'isEnabled' );
      buttonView.bind( 'isOn' ).to( command, 'value', value => !!value );

      // Execute the command when the button is clicked.
      this.listenTo(buttonView, 'execute', () =>
        editor.execute('readmoreremove', {})
      );

      return buttonView;
    });
  }

}
