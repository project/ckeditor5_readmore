/**
 * @file
 * Defines the ReadMoreCommand plugin.
 */

import { Command } from 'ckeditor5/src/core';
import {
  findElement,
} from "./utils";

/**
 * The ReadMore command.
 *
 * @extends module:core/command~Command
 */
export default class ReadMoreCommand extends Command {

  /**
   * @inheritDoc
   */
  refresh() {
    // Init the empty command value.
    this.value = null;

    // Find the element in the selection.
    const { selection } = this.editor.model.document;
    const El = findElement(selection, 'readmore');
    if (!El) {
      this.isEnabled = true;
    } else {
      this.isEnabled = false;
    }
  }

  /**
   * @inheritDoc
   */
  execute(values) {
    const { model } = this.editor;
    const editor = this.editor;

    const fragment = model.change((writer) => {
      // Find an existing readmore if it is being edited.
      var El = findElement(model.document.selection, 'readmore');
      if (El) {
        return;
      }

      const options = this.editor.config.get('ckeditor5_readmore');

      El = writer.createElement('readmore');
      writer.setAttributes({
        class: 'ckeditor-readmore',
        'data-readmore-less-text': options.less_text,
        'data-readmore-more-text': options.more_text,
        'data-readmore-type': options.type
      }, El);

      const selectionPool = [];
      const removePool = [];
      let i = 0;

      for ( const element of model.document.selection.getSelectedBlocks() ) {
        writer.append( writer.cloneElement(element, true), El )
        if (i) {
          removePool.push(element);
        } else {
          selectionPool.push(writer.createRangeOn(element));
        }
        i ++;
      }

      for (const element of removePool) {
        writer.remove(element)
      }

      writer.setSelection(selectionPool);
      return El;
    });
    editor.model.insertContent(fragment);
  }
}
