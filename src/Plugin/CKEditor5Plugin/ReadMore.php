<?php

namespace Drupal\ckeditor5_readmore\Plugin\CKEditor5Plugin;

use Drupal\ckeditor5\Plugin\CKEditor5PluginConfigurableInterface;
use Drupal\ckeditor5\Plugin\CKEditor5PluginConfigurableTrait;
use Drupal\ckeditor5\Plugin\CKEditor5PluginDefault;
use Drupal\ckeditor5\Plugin\CKEditor5PluginElementsSubsetInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\EditorInterface;

/**
 * CKEditor 5 ReadMore plugin configuration.
 *
 * @internal
 *   Plugin classes are internal.
 */
class ReadMore extends CKEditor5PluginDefault implements CKEditor5PluginConfigurableInterface, CKEditor5PluginElementsSubsetInterface {

  use CKEditor5PluginConfigurableTrait;

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Type of read more element'),
      '#description' => $this->t('Choose between plain text and button'),
      '#required' => TRUE,
      '#options' => [
        'text' => $this->t('Plain text'),
        'button' => $this->t('Button'),
      ],
      '#default_value' => $this->configuration['type'] ?? 'text',
    ];

    $form['more_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text in read more element'),
      '#description' => $this->t('This text shows up in read more element'),
      '#required' => TRUE,
      '#size' => 60,
      '#maxlength' => 128,
      '#default_value' => $this->configuration['more_text'] ?? $this->t('Read more'),
    ];
    $form['less_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text in show less element'),
      '#description' => $this->t('This text shows up in show less element'),
      '#required' => TRUE,
      '#size' => 60,
      '#maxlength' => 128,
      '#default_value' => $this->configuration['less_text'] ?? $this->t('Show less'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    ;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['type'] = $form_state->getValue('type');
    $this->configuration['more_text'] = $form_state->getValue('more_text');
    $this->configuration['less_text'] = $form_state->getValue('less_text');
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'type' => 'text',
      'more_text' => 'Read more',
      'less_text' => 'Show less',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getElementsSubset(): array {
    return ['div'];
  }

  /**
   * {@inheritdoc}
   */
  public function getDynamicPluginConfig(array $static_plugin_config, EditorInterface $editor): array {
    return [
      'ckeditor5_readmore' => $this->configuration,
    ];
  }

}
